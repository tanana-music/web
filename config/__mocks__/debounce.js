const debounce = jest.genMockFromModule('debounce')

debounce.mockImplementation(fn => fn)

module.exports = debounce
