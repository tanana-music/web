import sleep from './sleep'

export function fadeIn (el, ms, timing = 'ease-in') {
  el.style.transitionDuration = `${ms}ms`
  el.style.transitionProperty = 'opacity'
  el.style.transitionTimingFunction = timing
  el.style.opacity = 1
  return sleep(ms)
}

export function fadeOut (el, ms, timing = 'ease-in') {
  el.style.transitionDuration = `${ms}ms`
  el.style.transitionProperty = 'opacity'
  el.style.transitionTimingFunction = timing
  el.style.opacity = 0
  return sleep(ms)
}
