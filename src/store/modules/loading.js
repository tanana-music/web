const state = {
  done: false,
  loading: false
}

const getters = {
  finished (state) {
    return !state.loading && state.done
  }
}

const actions = {
  start ({ commit }) {
    commit('setLoading', true)
  },
  finish ({ commit }) {
    commit('setDone', true)
    commit('setLoading', false)
  },
  reset ({ commit }) {
    commit('setLoading', false)
    commit('setDone', false)
  }
}
const mutations = {
  setLoading (state, loading) {
    state.loading = loading
  },
  setDone (state, done) {
    state.done = done
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
