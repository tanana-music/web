import {
  shallowMount
} from '@vue/test-utils'
import FullLogo from '@/components/logo/FullLogo.vue'

describe('FullLogo.vue', () => {
  it('renders expected output', () => {
    const wrapper = shallowMount(FullLogo)
    expect(wrapper.element).toMatchSnapshot()
  })
})
