import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import DownloadXML from '@/components/DownloadXML.vue'
import loading from '@/store/modules/loading'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('DownloadXML.vue', () => {
  let actions
  let store

  beforeEach(() => {
    actions = {
      updateFileContent: jest.fn()
    }
    store = new Vuex.Store({
      modules: {
        file: {
          namespaced: true,
          actions
        },
        loading
      }
    })
    global.fetch = jest.fn(() => ({
      text: () => ''
    }))
  })

  it('renders expected output', () => {
    const wrapper = shallowMount(DownloadXML, { store, localVue })
    expect(wrapper.element).toMatchSnapshot()
  })
  it('dispatches "file/updateFileContent" when url is changed', async () => {
    const wrapper = shallowMount(DownloadXML, {
      store,
      localVue
    })
    const input = wrapper.find('input')
    input.setValue('test')
    await wrapper.vm.$nextTick()
    await wrapper.vm.$nextTick()
    expect(actions.updateFileContent).toHaveBeenCalled()
  })
  it('fetches content for given url', async () => {
    const wrapper = shallowMount(DownloadXML, { store, localVue })
    const input = wrapper.find('input')
    input.setValue('test')
    await wrapper.vm.$nextTick()
    expect(global.fetch).toHaveBeenCalledWith('test')
  })
})
