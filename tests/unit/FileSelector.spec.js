import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import FileSelector from '@/components/FileSelector.vue'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('FileSelector.vue', () => {
  let actions
  let store

  beforeEach(() => {
    actions = {
      updateFile: jest.fn()
    }
    store = new Vuex.Store({
      modules: {
        file: {
          namespaced: true,
          actions
        }
      }
    })
  })

  it('renders expected output', () => {
    const wrapper = shallowMount(FileSelector, { store, localVue })
    expect(wrapper.element).toMatchSnapshot()
  })
  // from https://stackoverflow.com/questions/48993134/how-to-test-input-file-with-jest-and-vue-test-utils
  // this needs to be skiped because jsdom, AGAIN
  it.skip('dispatches "file/updateFile" when input file is changed', () => {
    const wrapper = shallowMount(FileSelector, { store, localVue })
    const input = wrapper.find('input')
    const data = new ClipboardEvent('').clipboardData || new DataTransfer()
    data.items.add(new File(['foo'], 'never-gonna-give-you-up.xml'))
    input.element.files = data.files
    input.trigger('change')
    expect(actions.updateFile).toHaveBeenCalled()
  })
})
